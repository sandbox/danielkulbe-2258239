<?php

/**
 * @file
 * Admin page callbacks for the parallax block module.
 */

module_load_include('inc', 'block', 'block.admin');

/**
 * Form constructor for the add block form.
 *
 * @see parallax_block_add_block_form_validate()
 * @see parallax_block_add_block_form_submit()
 * @ingroup forms
 */
function parallax_block_add_block_form($form, &$form_state) {
  $form = block_admin_configure($form, $form_state, 'parallax_block', NULL);
  $form['#validate'] = array('parallax_block_validate', 'block_add_block_form_validate', 'parallax_block_add_block_form_validate');
  return $form;
}

/**
 * Form validation handler for parallax_block_add_block_form().
 *
 * @see parallax_block_add_block_form()
 * @see parallax_block_add_block_form_submit()
 */
function parallax_block_add_block_form_validate($form, &$form_state) {
  $parallax_block_exists = (bool) db_query_range('SELECT 1 FROM {parallax_block} WHERE info = :info', 0, 1, array(':info' => $form_state['values']['info']))->fetchField();

  if (empty($form_state['values']['info']) || $parallax_block_exists) {
    form_set_error('info', t('Ensure that each block description is unique.'));
  }
}

/**
 * Form submission handler for parallax_block_add_block_form().
 *
 * Saves the new custom block.
 *
 * @see parallax_block_add_block_form()
 * @see parallax_block_add_block_form_validate()
 */
function parallax_block_add_block_form_submit($form, &$form_state) {
  $background = file_load($form_state['values']['image_field']);
  $background->status = FILE_STATUS_PERMANENT;
  file_save($background);

  $delta = db_insert('parallax_block')
    ->fields(array(
      'info'              => $form_state['values']['info'],
      'body'              => $form_state['values']['body']['value'],
      'body_format'       => $form_state['values']['body']['format'],
      'background'        => $form_state['values']['image_field'],
      'background_format' => $form_state['values']['image_format'],
    ))
    ->execute();
  // Store block delta to allow other modules to work with new block.
  $form_state['values']['delta'] = $delta;
  file_usage_add($background, 'parallax_block', 'background', $delta);

  $query = db_insert('block')->fields(array('visibility', 'pages', 'custom', 'title', 'module', 'theme', 'status', 'weight', 'delta', 'cache'));
  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      $query->values(array(
        'visibility' => (int) $form_state['values']['visibility'],
        'pages' => trim($form_state['values']['pages']),
        'custom' => (int) $form_state['values']['custom'],
        'title' => $form_state['values']['title'],
        'module' => $form_state['values']['module'],
        'theme' => $theme->name,
        'status' => 0,
        'weight' => 0,
        'delta' => $delta,
        'cache' => DRUPAL_NO_CACHE,
      ));
    }
  }
  $query->execute();

  $query = db_insert('block_role')->fields(array('rid', 'module', 'delta'));
  foreach (array_filter($form_state['values']['roles']) as $rid) {
    $query->values(array(
      'rid' => $rid,
      'module' => $form_state['values']['module'],
      'delta' => $delta,
    ));
  }
  $query->execute();

  // Store regions per theme for this block
  foreach ($form_state['values']['regions'] as $theme => $region) {
    db_merge('block')
      ->key(array('theme' => $theme, 'delta' => $delta, 'module' => $form_state['values']['module']))
      ->fields(array(
        'region' => ($region == BLOCK_REGION_NONE ? '' : $region),
        'pages' => trim($form_state['values']['pages']),
        'status' => (int) ($region != BLOCK_REGION_NONE),
      ))
      ->execute();
  }

  drupal_set_message(t('The block has been created.'));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
}

/**
 * Form constructor for the parallax block deletion form.
 *
 * @param $delta
 *   The unique ID of the block within the context of $module.
 *
 * @see parallax_block_block_delete_submit()
 */
function parallax_block_block_delete($form, &$form_state, $delta) {
  $block = block_load('parallax_block', $delta);
  $parallax_block = parallax_block_block_get($block->delta);
  $form['info'] = array('#type' => 'hidden', '#value' => $parallax_block['info'] ? $parallax_block['info'] : $parallax_block['title']);
  $form['bid'] = array('#type' => 'hidden', '#value' => $block->delta);

  return confirm_form($form, t('Are you sure you want to delete the block %name?', array('%name' => $parallax_block['info'])), 'admin/structure/block', '', t('Delete'), t('Cancel'));
}

/**
 * Form submission handler for parallax_block_block_delete().
 *
 * @see parallax_block_block_delete()
 */
function parallax_block_block_delete_submit($form, &$form_state) {
  $fid  = db_query("SELECT background FROM {parallax_block} WHERE bid = :bid", array(':bid' => $form_state['values']['bid']))->fetchField();
  $file = file_load($fid);

  db_delete('parallax_block')
    ->condition('bid', $form_state['values']['bid'])
    ->execute();
  db_delete('block')
    ->condition('module', 'parallax_block')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();
  db_delete('block_role')
    ->condition('module', 'parallax_block')
    ->condition('delta', $form_state['values']['bid'])
    ->execute();

  file_delete($file);
  file_usage_delete($file, 'parallax_block', 'background', $form_state['values']['bid']);

  drupal_set_message(t('The block %name has been removed.', array('%name' => $form_state['values']['info'])));
  cache_clear_all();
  $form_state['redirect'] = 'admin/structure/block';
  return;
}
