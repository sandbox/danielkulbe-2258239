<?php

/**
 * @file
 * Install, update, uninstall for the Parallax Block module.
 */

/**
 * Implements hook_schema().
 */
function parallax_block_schema() {
  $schema['parallax_block'] = array(
    'description' => 'Stores contents of parallax blocks.',
    'fields' => array(
      'bid' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => "The block's {block}.bid.",
      ),
      'info' => array(
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Block description.',
      ),
      'body' => array(
        'type' => 'text',
        'not null' => FALSE,
        'size' => 'big',
        'description' => 'Block contents.',
        'translatable' => TRUE,
      ),
      'background' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The background image file {files}.fid.',
      ),
      'body_format' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'description' => 'The {filter_format}.format of the block body.',
      ),
      'background_format' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
        'description' => 'The {image_styles}.format of the block background image.',
      ),
    ),
    'unique keys' => array(
      'info' => array('info'),
    ),
    'indexes' => array(
      'background' => array('background'),
    ),
    'primary key' => array('bid'),
  );
  return $schema;
}

/**
 * Implements hook_install().
 */
function parallax_block_install() {
  $dir_docs_created = drupal_mkdir('public://background/');
  if ($dir_docs_created === FALSE) {
  drupal_set_message(t('The directory public://background/ could not be created. Please contact your site administrator.'), 'warning');
  }
}
