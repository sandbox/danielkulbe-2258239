  ___               _ _            ___ _         _
 | _ \__ _ _ _ __ _| | |__ ___ __ | _ ) |___  __| |__
 |  _/ _` | '_/ _` | | / _` \ \ / | _ \ / _ \/ _| / /
 |_| \__,_|_| \__,_|_|_\__,_/_\_\ |___/_\___/\__|_\_\

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Credits


INTRODUCTION
------------

This module implements adds a block type with popular background-image
parallax effect.

INSTALLATION
------------

1. Copy the parallax directory to your sites/SITENAME/modules directory.

2. Enable the module at Administer >> Site building >> Modules.


CREDITS
-------
* Daniel Kulbe (dku)
