<?php
/**
 * @file
 * Adds a block with popular background image parallax effect.
 */

/**
 * Implementation of hook_theme()
 */
function parallax_block_theme() {
  return array(
    'block__parallax_block' => array(
      'render element' => 'elements',
      'template' => 'parallax-block',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function parallax_block_menu() {
  $default_theme = variable_get('theme_default', 'bartik');
  $items['admin/structure/block/manage/parallax_block/%'] = array(
    'title' => 'Configure block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('block_admin_configure', 'parallax_block', 5),
    'access arguments' => array('administer blocks'),
    'file' => 'block.admin.inc',
    'file path' => drupal_get_path('module', 'block'),
  );
  $items['admin/structure/block/manage/parallax_block/%/configure'] = array(
    'title' => 'Configure parallax block',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'context' => MENU_CONTEXT_INLINE,
  );
  $items['admin/structure/block/manage/parallax_block/%/delete'] = array(
    'title' => 'Delete block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('parallax_block_block_delete', 5),
    'access arguments' => array('administer blocks'),
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_NONE,
    'file' => 'parallax_block.admin.inc',
  );
  $items['admin/structure/block/parallax_add'] = array(
    'title' => 'Add parallax block',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('parallax_block_add_block_form'),
    'access arguments' => array('administer blocks'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'parallax_block.admin.inc',
  );
  foreach (list_themes() as $key => $theme) {
    if ($key != $default_theme) {
      $items['admin/structure/block/list/' . $key . '/parallax_add'] = array(
        'title' => 'Add parallax block',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('parallax_block_add_block_form'),
        'access arguments' => array('administer blocks'),
        'type' => MENU_LOCAL_ACTION,
        'file' => 'parallax_block.admin.inc',
      );
    }
  }
  return $items;
}

/**
 * Implements hook_block_info().
 */
function parallax_block_block_info () {
  $blocks = array();

  $result = db_query('SELECT bid, info FROM {parallax_block} ORDER BY info');
  foreach ($result as $block) {
    $blocks[$block->bid] = array(
      'info'   => t('Parallax Block') . ': ' . $block->info,
      'region' => 'content',
      'status' => 1,
      'cache'  => DRUPAL_NO_CACHE,
      'theme'  => 'parallax_block',
    );
  }
  return $blocks;
}

/**
 * Implements hook_form_FORM_ID_alter().
 *  - FORM_ID: block_admin_configure
 */
function parallax_block_form_block_admin_configure_alter($form, &$form_state) {
  if ($form['module']['#value'] == 'parallax_block') {
    $form['#attributes']['enctype'] = 'multipart/form-data';
    $form['#validate'][] = 'parallax_block_validate';
  }
}

/**
 * Implements hook_block_configure().
 */
function parallax_block_block_configure($delta = 0) {
  if ($delta) {
    $parallax_block = parallax_block_block_get($delta);
  }
  else {
    $parallax_block = array();
  }
  return parallax_block_block_form($parallax_block);
}

/**
 * Returns information from database about a parallax block.
 *
 * @param $bid
 *   ID of the block to get information for.
 *
 * @return
 *   Associative array of information stored in the database for this block.
 *   Array keys:
 *   - bid: Block ID.
 *   - info: Block description.
 *   - body: Block contents.
 *   - body_format: Filter ID of the filter format for the body.
 *   - background: File ID of the image for the background.
 *   - background_format: Style ID of the image style format for the background.
 */
function parallax_block_block_get($bid) {
  return db_query("SELECT * FROM {parallax_block} WHERE bid = :bid", array(':bid' => $bid))->fetchAssoc();
}

/**
 * Form constructor for the parallax block form.
 *
 * @param $edit
 *   (optional) An associative array of information retrieved by
 *   parallax_block_get_block() if an existing block is being edited, or an empty
 *   array otherwise. Defaults to array().
 *
 * @ingroup forms
 */
function parallax_block_block_form($edit = array()) {
  $edit += array(
    'info'       => '',
    'body'       => '',
  );
  $form['info'] = array(
    '#type' => 'textfield',
    '#title' => t('Block description'),
    '#default_value' => $edit['info'],
    '#maxlength' => 64,
    '#description' => t('A brief description of your block. Used on the <a href="@overview">Blocks administration page</a>.', array('@overview' => url('admin/structure/block'))),
    '#required' => TRUE,
    '#weight' => -18,
  );
  $form['image_field'] = array(
    '#title' => t('Block background'),
    '#type' => 'managed_file',
    '#description' => t('The uploaded image will be displayed as background using the image style choosen below.'),
    '#default_value' => isset($edit['background']) ? $edit['background'] : '',
    '#upload_validators' => array(
      'file_validate_extensions' => array('png gif jpg jpeg'),
    ),
    '#upload_location' => 'public://background/',
    '#required' => TRUE,
    '#weight' => -17,
    '#attributes' => array('class' => array('parallax-block-image-field')),
  );
  // If there is already an uploaded image display the image here.
  if (isset($edit['background']) && is_numeric($edit['background'])) {
    $image = file_load($edit['background']);
    $form['image_field']['#field_prefix'] = theme('image_style', array('path' => $image->uri, 'style_name' => 'thumbnail', 'width' => 120, 'height' => 80));
  }
  $form['image_format'] = array(
    '#title' => t('Background image style'),
    '#type' => 'select',
    '#description' => t('Choose an image style to use when displaying this background image.'),
    '#options' => image_style_options(TRUE),
    '#default_value' => isset($edit['background_format']) ? $edit['background_format'] : '',
    '#required' => TRUE,
    '#weight' => -16,
  );
  $form['body_field'] = array(
    '#weight' => -15,
    'body' => array(
      '#type' => 'text_format',
      '#title' => t('Block body'),
      '#default_value' => $edit['body'],
      '#format' => isset($edit['body_format']) ? $edit['body_format'] : '',
      '#rows' => 15,
      '#description' => t('The content of the block as shown to the user.'),
      '#required' => TRUE,
      '#weight' => -16,
    ),
  );
  $form['#attached']['css'] = array(drupal_get_path('module', 'parallax_block') . '/css/parallax_block.admin.css');

  return $form;
}

/**
 * Verifies that the user supplied an image with the form.
 *
 * @ingroup parallax_block
 */
function parallax_block_validate ($form, &$form_state) {
  if (!isset($form_state['values']['image_field']) || !is_numeric($form_state['values']['image_field'])) {
    form_set_error('image_field', t('Please select an image to upload.'));
  }
}

/**
 * Saves a user-created block in the database.
 *
 * @param $delta
 *   Block ID of the block to save.
 * @param $edit
 *   Associative array of fields to save. Array keys:
 *   - info: Block description.
 *   - image_field: Image file ID.
 *   - image_format: Image style format.
 *   - body: Associative array of body value and format.  Array keys:
 *     - value: Block contents.
 *     - format: Filter ID of the filter format for the body.
 *
 * @return
 *   Always returns TRUE.
 */
function parallax_block_block_save($delta = '', $edit = array()) {
  // remove old background ón update
  $block = parallax_block_block_get($delta);
  if ($block['background'] != $edit['image_field']) {
    $old = file_load($block['background']);
    file_delete($old);
    file_usage_delete($old, 'parallax_block', 'background', $delta);
  }

  // save new image to system
  $background = file_load($edit['image_field']);
  $background->status = FILE_STATUS_PERMANENT;
  file_save($background);
  file_usage_add($background, 'parallax_block', 'background', $delta);

  // save block with background image relation
  db_update('parallax_block')
    ->fields(array(
      'info'               => $edit['info'],
      'body'               => $edit['body']['value'],
      'body_format'        => $edit['body']['format'],
      'background'         => $background->fid,
      'background_format'  => $edit['image_format'],
    ))
    ->condition('bid', $delta)
    ->execute();

  return TRUE;
}


/**
 * Implements hook_form_FORM_ID_alter().
 *  - FORM_ID: bblock_admin_display_form
 */
function parallax_block_form_block_admin_display_form_alter (&$form, &$form_state) {
  foreach ($form['blocks'] as $key => $element) {
    if ($element['module']['#value'] == 'parallax_block') {
      $form['blocks'][$key]['delete'] = array(
        '#type' => 'link',
        '#title' => t('delete'),
        '#href' => 'admin/structure/block/manage/parallax_block/' . $element['delta']['#value'] . '/delete',
     );
    }
  }
}

/**
 * Implements hook_block_view().
 *
 * Generates the parallax blocks for display.
 */
function parallax_block_block_view($delta = '') {
  $block = db_query('SELECT p.body, p.body_format, p.background_format, i.filename, i.uri, i.filemime, i.filesize FROM {parallax_block} p LEFT JOIN {file_managed} i ON i.fid = p.background WHERE p.bid = :bid', array(':bid' => $delta))->fetchObject();

  $data['subject']    = NULL;
  $data['background'] = image_style_url($block->background_format, $block->uri);
  $data['content']    = check_markup($block->body, $block->body_format, '', TRUE);

  return $data;
}

/**
 * Implements hook_preprocess_block().
 *
 * Processes special variables for parallax-block.tpl.php.
 *
 * Most themes utilize their own copy of parallax-block.tpl.php. The default
 * is located inside "sites/SITENAME/parallax_block/parallax-block.tpl.php".
 * Look in there for the full list of variables.
 *
 * The $variables array contains the following arguments:
 * - $block
 *
 * @see parallax-block.tpl.php
 */
function parallax_block_preprocess_block(&$variables) {
  $block = $variables['elements']['#block'];

  if ($block->module == 'parallax_block') {
    // Create the $background variable that templates expect.
    $variables['background'] = $block->background;

    // Add CSS with parallax block settings
    $style_file = drupal_get_path('module','parallax_block') . '/css/parallax_block.css';
    drupal_add_css($style_file, array('type' => 'file', 'group' => CSS_DEFAULT, 'weight' => 3));
    drupal_add_css('#'.$variables['block_html_id'].'{background-image:url(' . $block->background . ');}', array('type' => 'inline', 'group' => CSS_DEFAULT, 'weight' => 3));
  }
}
